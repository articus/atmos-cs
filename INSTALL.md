# Install

32-bit devices are currently not supported.

## Dependencies
Atmos currently (2020-06-14) requires the following libraries:
* libepoxy	(1.5-4.1) // Should be included with gtkmm-3.0 package.
* glfw3		(3.3.2-1)
* gtkmm-3.0	(3.24.2-1)

## Windows
Currently, building and installing Atmos on Windows is only possible through MSYS2. Please find installation instructions [here](https://www.msys2.org/wiki/MSYS2-installation/). If Chocolatey is available on the system, you may instead use `choco install msys2`.

Either way, remember to update MSYS2 before running the build script, or you will get errors during build. To do this, open "MSYS2 MinGW 64-bit" (or mingw64.exe in the MSYS2 install directory) and run `pacman -Syu`, confirming everything.

If you have installed MSYS2 in a custom directory, set the MSYSDIR environmental variable before running INSTALL.bat: `set MSYSDIR=<custom msys2 directory here>` (without the `< >`). The INSTALL script will otherwise attempt to locate the directory containing msys2_shell.cmd automatically, and will fallback to prompting the user.

If all is done correctly, running INSTALL.bat in Atmos' source directory should build the entire project without errors.


## Unix
After installing all required packages, a simple `cmake -B build && cmake --build build` should suffice.
