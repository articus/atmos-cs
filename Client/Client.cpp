#include "Atmos/Atmos.h"
#include "Atmos/Objects/Dump.h"
#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include <string.h>

#include <GLFW/glfw3.h>

#include <readline/readline.h>
#include <readline/history.h>

#include <iostream>
#include <string>
#include <thread>
#include <filesystem>

/* TODO:
- Events
- Layers
- Website (ExpressJS, Vue, SQLite)
- Octrees (AABB)
- Ray
- RenderSystem
- Camera
- Serialization
- Networking
- GTKMM - (Windows: MSYS2, works)
*/

int main(int argc, char** argv)
{
	std::filesystem::current_path(std::filesystem::path(argv[0]).parent_path());

    int error;
    lua_State* L = lua_open();
    luaL_openlibs(L);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(800, 600, "legitimerately had no idea this would even work tbh", NULL, NULL);
    
    glfwMakeContextCurrent(window);
    std::shared_ptr<Atmos::Instance> instance = Atmos::ObjectManager::Create<Atmos::Instance>("Custom name", "custom classname");
    instance->Push(L);
    lua_setglobal(L, "test");

	std::shared_ptr<Atmos::Part> part = Atmos::ObjectManager::Create<Atmos::Part>();
	part->Push(L);
	lua_setglobal(L, "part");
	part->SetParent(instance);

	std::shared_ptr<Atmos::Script> script = Atmos::ObjectManager::Create<Atmos::Script>();
	script->Compile(L, "print(script.Parent)");
	script->SetParent(part);
	script->Run(L);
	
	rapidjson::Document Dump = Atmos::Dump(instance);
	rapidjson::StringBuffer buffer;
	rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
	Dump.Accept(writer);

	std::cout << buffer.GetString() << std::endl;

	const auto& i = Atmos::Undump(Dump);
	const std::shared_ptr<Atmos::Script> scr = std::dynamic_pointer_cast<Atmos::Script>(i[2]);
	scr->Run(L);
	
	for(auto& c : i)
		std::cout << c->Name;
	char* buf; 
	std::thread otherthread([&]{ while ((buf = readline("> ")) != nullptr) {
		if(strlen(buf)) add_history(buf);
        error = luaL_loadbuffer(L, buf, strlen(buf), "client") || lua_pcall(L, 0, 0, 0);
        if (error) {
            fprintf(stderr, "%s\n", lua_tostring(L, -1));
            lua_pop(L, 1);
        }
		free(buf);
    } });

    while(!glfwWindowShouldClose(window))
    {
        if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, true);
        
        glClearColor(1,192/255.f,203/255.f, 1);
        glClear(GL_COLOR_BUFFER_BIT);

        auto renderChildren = [](std::shared_ptr<Atmos::Instance> instance, const auto rndFunc) -> void {
            instance->Render();
            for(std::shared_ptr<Atmos::Instance>& child : instance->Children)
                rndFunc(child, rndFunc);
        };

        renderChildren(instance, renderChildren);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwTerminate();

    lua_close(L);
    return 0;
}
