@ECHO OFF
SETLOCAL EnableDelayedExpansion

REM Can't use exit /B with CALLs so instead just copying out code.

IF DEFINED MSYSDIR IF EXIST !MSYSDIR!\msys2_shell.cmd (GOTO :Setup) ELSE (ECHO %%MSYSDIR%%: !MSYSDIR! not valid path!)

ECHO Attempting to automatically locate MSYS2 install...
ECHO Trying C:\msys64... [Default location]
SET MSYSDIR=C:\msys64
IF EXIST !MSYSDIR!\msys2_shell.cmd GOTO :Setup
ECHO Trying C:\tools\msys64... [Chocolatey]
SET MSYSDIR=C:\tools\msys64
IF EXIST !MSYSDIR!\msys2_shell.cmd GOTO :Setup

ECHO Could not find MSYS2...
:InputMsysDir
SET /P MSYSDIR=Please enter directory containing msys2_shell.cmd: 
IF EXIST !MSYSDIR!\msys2_shell.cmd GOTO :Setup
GOTO :InputMsysDir

:Setup
!MSYSDIR!\msys2_shell.cmd -mingw64 -here -no-start -defterm -c "./INSTALL.msys2"
EXIT /B 0
