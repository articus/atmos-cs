#include "Atmos/Objects/Events/ChildAdded.h"

namespace Atmos
{
std::list<Atmos::Event::Handler>::const_iterator ChildAddedEvent::Subscribe(Handler handler)
{
    return Handlers.emplace(Handlers.end(), handler);
}

void ChildAddedEvent::Remove(std::list<Atmos::Event::Handler>::const_iterator iter)
{
    Handlers.erase(iter);
}

void ChildAddedEvent::Handle()
{
    for(std::list<Atmos::Event::Handler>::const_iterator handler = Handlers.begin(); handler != Handlers.end(); ++handler)
        if(!(*handler)(this))
            Handlers.erase(handler);
}


}
