#include "Atmos/Objects/Part.h"

namespace Atmos {

Part::Part()
    : Instance("Part", "Part")
{
    Setup();
};

Part::Part(const std::string& name)
    : Instance(name, "Part")
{
    Setup();
};

Part::Part(rapidjson::Value& obj)
	: Instance(obj),
	  Size(obj["Size"]),
	  Position(obj["Position"]),
	  Rotation(obj["Rotation"]),
	  Color(obj["Color"])
{
	Setup();
};

void Part::Setup()
{
    static constexpr float vertices[] = {
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        0.0f, 0.5f, 0.0f
    };

    struct Test {
        Test()
        {
			GEN_REGISTRAR(Part);

            unsigned int vertexShader, fragmentShader;
            vertexShader = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vertexShader, 1, &VertexShaderSource, NULL);
            glCompileShader(vertexShader);
            fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fragmentShader, 1, &FragmentShaderSource, NULL);
            glCompileShader(fragmentShader);
            ShaderProgram = glCreateProgram();
            glAttachShader(ShaderProgram, vertexShader);
            glAttachShader(ShaderProgram, fragmentShader);
            glLinkProgram(ShaderProgram);
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);

            glGenVertexArrays(1, &VAO);
            glGenBuffers(1, &VBO);

            glBindVertexArray(VAO);

            glBindBuffer(GL_ARRAY_BUFFER, VBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);

            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glBindVertexArray(0);
        };
    };

    static Test test;
}

int Part::SizeGetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    return part->Size.Push(L);
}

int Part::SizeSetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    part->Size = RawGetFromIndex<Vector3>(L, 3);
    return 0;
}

int Part::PositionGetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    return part->Position.Push(L);
}

int Part::PositionSetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    part->Position = RawGetFromIndex<Vector3>(L, 3);
    return 0;
}

int Part::RotationGetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    return part->Rotation.Push(L);
}

int Part::RotationSetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    part->Rotation = RawGetFromIndex<Vector3>(L, 3);
    return 0;
}

int Part::ColorGetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    return part->Color.Push(L);
}

int Part::ColorSetHandler(lua_State* L)
{
    std::shared_ptr<Part>& part = GetFromIndex<Part>(L, 1);
    part->Color = RawGetFromIndex<Color3>(L, 3);
    return 0;
}

void Part::Render()
{
    glUseProgram(ShaderProgram);
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void Part::Dump(rapidjson::Value& object, rapidjson::Document::AllocatorType& allocator)
{
	Instance::Dump(object, allocator);

	rapidjson::Value SizeJS = Size.Dump(allocator);
	rapidjson::Value PositionJS = Position.Dump(allocator);
	rapidjson::Value RotationJS = Rotation.Dump(allocator);
	rapidjson::Value ColorJS = Color.Dump(allocator);
	object.AddMember("Size", SizeJS, allocator);
	object.AddMember("Position", PositionJS, allocator);
	object.AddMember("Rotation", RotationJS, allocator);
	object.AddMember("Color", ColorJS, allocator);
}
}
