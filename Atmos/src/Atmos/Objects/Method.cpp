#include "Atmos/Objects/Method.h"

namespace Atmos {
int Method::Wrapper(lua_State* L)
{
    std::function<int(lua_State*)>& func = **static_cast<std::function<int(lua_State*)>**>(lua_touserdata(L, lua_upvalueindex(1)));
    return func(L);
}
void Method::Register(lua_State* L) const
{
    *static_cast<const std::function<int(lua_State*)>**>(lua_newuserdata(L, sizeof(std::function<int(lua_State)>*))) = &Func;
    lua_pushcclosure(L, Wrapper, 1);
    lua_setfield(L, -2, Name.c_str());
}
} // namespace Atmos
