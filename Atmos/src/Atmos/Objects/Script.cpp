#include "Atmos/Objects/Script.h"

#include <iostream>

namespace Atmos
{

Script::Script()
	: Instance("Script", "Script")
{
};

Script::Script(const std::string& name)
	: Instance(name, "Script")
{
};

Script::Script(rapidjson::Value& obj)
	: Instance(obj)
	, Bytecode(obj["Bytecode"].GetString(), obj["Bytecode"].GetStringLength())
{
};
	

int Script::Run(lua_State* L)
{
	lua_State* scriptThread = lua_newthread(L);
	lua_pop(L, 1);
	this->Push(scriptThread);
	lua_setglobal(scriptThread, "script");
	luaL_loadbuffer(scriptThread, Bytecode.data(), Bytecode.size(), Name.data());
	int error = lua_pcall(scriptThread, 0, LUA_MULTRET, 0);
	if (error)
		std::cout << lua_tostring(scriptThread, -1) << std::endl;
	lua_pushnil(L);
	lua_setglobal(L, "script"); //shares scriptThread globals, so must be reset.
	return 0;
}

int Script::writer(lua_State* L, const void* lBuffer, size_t sizeLBuffer, void* oBuffer)
{
	((std::string*)oBuffer)->append((char*)lBuffer, sizeLBuffer);
	return 0;
}

int Script::Compile(lua_State* L, std::string source)
{
	luaL_loadstring(L, source.c_str());
	int error = lua_dump(L, &Script::writer, (void*)&Bytecode);
	if (error)
		std::cerr << Name << " was not compiled. Error: " << lua_tostring(L, -1) << std::endl;
	lua_settop(L, 0);
	return 0;
}

void Script::Dump(rapidjson::Value& object, rapidjson::Document::AllocatorType& allocator)
{
	Instance::Dump(object, allocator);

	rapidjson::Value BytecodeJS(rapidjson::kStringType);
	BytecodeJS.SetString(Bytecode.data(), Bytecode.size(), allocator);
	object.AddMember("Bytecode", BytecodeJS, allocator);
}
}
