#include "Atmos/Objects/Vector3.h"

#include <cmath>

namespace Atmos {
Vector3::Vector3(Number xa, Number ya, Number za)
    : x(xa)
    , y(ya)
    , z(za) {};

Vector3::Vector3(rapidjson::Value& obj)
	: x(obj[0].GetDouble())
	, y(obj[1].GetDouble())
	, z(obj[2].GetDouble())
{};

Vector3 operator+(const Vector3& lhs, const Vector3& rhs) { return { lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z }; }
Vector3 operator-(const Vector3& lhs, const Vector3& rhs) { return { lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z }; }
template <typename T>
Vector3 operator*(const Vector3& lhs, const T& rhs) { return { lhs.x * rhs, lhs.y * rhs, lhs.z * rhs }; }
Vector3 operator*(const Vector3& lhs, const Vector3& rhs) { return { lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z }; }
template <typename T>
Vector3 operator/(const Vector3& lhs, const T& rhs) { return { lhs.x / rhs, lhs.y / rhs, lhs.z / rhs }; }
Vector3 operator/(const Vector3& lhs, const Vector3& rhs) { return { lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z }; }
bool operator==(const Vector3& lhs, const Vector3& rhs) { return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z); }

void Vector3::SetupState(lua_State* L)
{
    /* Metatable */
    luaL_newmetatable(L, MetatableName);

    lua_pushcfunction(L, &Vector3::__index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, &Vector3::__newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, &Vector3::__tostring);
    lua_setfield(L, -2, "__tostring");
    lua_pushcfunction(L, &Vector3::__eq);
    lua_setfield(L, -2, "__eq");
    lua_pushcfunction(L, &Vector3::__add);
    lua_setfield(L, -2, "__add");
    lua_pushcfunction(L, &Vector3::__sub);
    lua_setfield(L, -2, "__sub");
    lua_pushcfunction(L, &Vector3::__mul);
    lua_setfield(L, -2, "__mul");
    lua_pushcfunction(L, &Vector3::__div);
    lua_setfield(L, -2, "__div");

    for (const auto& [name, method] : Methods)
        method.Register(L);

    lua_pop(L, 1);

    /* Vector3 Library */
    lua_newtable(L);
    lua_pushcfunction(L, &Vector3::newVector3);
    lua_setfield(L, -2, "new");
    lua_setglobal(L, "Vector3");
}

int Vector3::Push(lua_State* L)
{
    PushRaw(L, this);
    GetMetatable<Vector3>(L);
    lua_setmetatable(L, -2);
    return 1;
}

int Vector3::newVector3(lua_State* L)
{
    return Vector3(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3)).Push(L);
}

/* Methods */

Number Vector3::Magnitude()
{
    return std::sqrt(std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2));
}

Vector3 Vector3::Unit()
{
    return (*this) / Magnitude();
}

Vector3 Vector3::Lerp(Vector3 goal, Number alpha)
{
    return (*this) + (goal - *this) * alpha;
}

int Vector3::Lerp(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 goal = RawGetFromIndex<Vector3>(L, 1);
    Number alpha = lua_tonumber(L, 3);
    return vec3.Lerp(goal, alpha).Push(L);
}

Number Vector3::Dot(Vector3 other)
{
    return x * other.x + y * other.y + z * other.z;
}

int Vector3::Dot(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 other = RawGetFromIndex<Vector3>(L, 2);
    lua_pushnumber(L, vec3.Dot(other));
    return 1;
}

Vector3 Vector3::Cross(Vector3 other)
{
    return {
        y * other.z - z * other.y,
        z * other.x - x * other.z,
        x * other.y - y * other.x
    };
}

int Vector3::Cross(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 other = RawGetFromIndex<Vector3>(L, 2);
    return vec3.Cross(other).Push(L);
}
/* Metamethods */

int Vector3::__index(lua_State* L)
{
    static std::unordered_map<std::string, Number Vector3::*> fieldMap = {
        { "x", &Vector3::x },
        { "X", &Vector3::x },
        { "y", &Vector3::y },
        { "Y", &Vector3::y },
        { "z", &Vector3::z },
        { "Z", &Vector3::z },
    };
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    std::string field = lua_tostring(L, 2);
    const auto iter = fieldMap.find(field);
    if (iter != fieldMap.end()) {
        lua_pushnumber(L, vec3.*iter->second);
        return 1;
    } else if (field == "Magnitude") {
        lua_pushnumber(L, vec3.Magnitude());
        return 1;
    } else if (field == "Unit") {
        return vec3.Unit().Push(L);
    };
    const auto iter2 = Methods.find(field);
    if (iter2 != Methods.end()) {
        return iter2->second.Push<Vector3>(L);
    }
    return luaL_error(L, "%s is not a valid member of %s", field.c_str(), MetatableName);
}

int Vector3::__newindex(lua_State* L)
{
    return luaL_error(L, "%s cannot be assigned to", lua_tostring(L, 2));
}

int Vector3::__tostring(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    lua_pushfstring(L, "%f, %f, %f", vec3.x, vec3.y, vec3.z);
    return 1;
}

int Vector3::__eq(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 nvec3 = RawGetFromIndex<Vector3>(L, 2);
    lua_pushboolean(L, vec3 == nvec3);
    return 1;
}

int Vector3::__add(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 nvec3 = RawGetFromIndex<Vector3>(L, 2);
    return (vec3 + nvec3).Push(L);
}

int Vector3::__sub(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 nvec3 = RawGetFromIndex<Vector3>(L, 2);
    return (vec3 - nvec3).Push(L);
}

int Vector3::__mul(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 nvec3 = RawGetFromIndex<Vector3>(L, 2);
    return (vec3 * nvec3).Push(L);
}

int Vector3::__div(lua_State* L)
{
    Vector3 vec3 = RawGetFromIndex<Vector3>(L, 1);
    Vector3 nvec3 = RawGetFromIndex<Vector3>(L, 2);
    return (vec3 / nvec3).Push(L);
}

rapidjson::Value Vector3::Dump(rapidjson::Document::AllocatorType& allocator)
{
	rapidjson::Value Array(rapidjson::kArrayType);
	Array.PushBack(x, allocator);
	Array.PushBack(y, allocator);
	Array.PushBack(z, allocator);
	return Array;
}
} // namespace Atmos
