#include "Atmos/Objects/Property.h"

namespace Atmos {

int Property::DefaultGetHandler(lua_State* L)
{
    return luaL_error(L, "can't get value");
}

int Property::DefaultSetHandler(lua_State* L)
{
    return luaL_error(L, "can't set value");
}

Property::Property(const std::string& name, void* data, const std::function<int(lua_State*)>& getHandler, const std::function<int(lua_State*)>& setHandler)
    : Name(name)
    , Data(data)
    , GetHandler(getHandler ? getHandler : DefaultGetHandler)
    , SetHandler(setHandler ? setHandler : DefaultSetHandler) {};
}
