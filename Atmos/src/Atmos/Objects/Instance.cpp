#include "Atmos/Objects/Instance.h"

namespace Atmos {

Instance::Instance(const std::string& name)
    : Name(name) {};
Instance::Instance(const std::string& name, const std::string& className)
    : Name(name)
    , ClassName(className) {};
Instance::Instance(rapidjson::Value& obj)
	: Name(obj["Name"].GetString(), obj["Name"].GetStringLength())
	, ClassName(obj["ClassName"].GetString(), obj["ClassName"].GetStringLength()) {};

std::shared_ptr<Instance> Instance::GetPointer()
{
    return shared_from_this();
}

void Instance::AddChild(const std::shared_ptr<Instance>& child)
{
    if (child->Parent)
        child->Parent->RemoveChild(child);
    child->Parent = GetPointer();
    Children.emplace_back(child);
}

void Instance::RemoveChild(const std::shared_ptr<Instance>& child)
{
    auto iter = std::find(Children.begin(), Children.end(), child);
    if (iter != Children.end()) {
        child->Parent = nullptr;
        Children.erase(iter);
    }
}

void Instance::SetParent(const std::shared_ptr<Instance>& parent)
{
    if (parent)
        parent->AddChild(shared_from_this());
    else
        RemoveParent();
}

void Instance::RemoveParent()
{
    if (Parent)
        Parent->RemoveChild(shared_from_this());
}

const std::unordered_map<std::string, const Property> Instance::ConcatProperties(std::unordered_map<std::string, const Property> lhs, const std::unordered_map<std::string, const Property>& rhs)
{
    lhs.insert(rhs.begin(), rhs.end());
    return lhs;
}

void Instance::SetupState(lua_State* L)
{
    luaL_newmetatable(L, Instance::MetatableName);

    lua_pushcfunction(L, &Instance::__index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, &Instance::__newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, &Instance::__tostring);
    lua_setfield(L, -2, "__tostring");
    lua_pushcfunction(L, &Instance::__eq);
    lua_setfield(L, -2, "__eq");
    lua_pushcfunction(L, &Instance::__gc);
    lua_setfield(L, -2, "__gc");

    for (const auto& [name, method] : Methods)
        method.Register(L);

    lua_pop(L, 1);

    lua_newtable(L);
    lua_pushcfunction(L, &Instance::newInstance);
    lua_setfield(L, -2, "new");
    lua_setglobal(L, "Instance");
}

int Instance::Push(lua_State* L)
{
    new (lua_newuserdata(L, sizeof(std::shared_ptr<Instance>))) std::shared_ptr(shared_from_this());
    GetMetatable<Instance>(L);
    lua_setmetatable(L, -2);
    return 1;
}

int Instance::newInstance(lua_State* L)
{
    std::string field = lua_tostring(L, 1);
    auto iter = newRegistrars.find(field);
    if (iter != newRegistrars.end())
        return iter->second(L);
    return luaL_error(L, "Unable to create an Instance of type \"%s\"", field.c_str());
}

int Instance::NameGetHandler(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    lua_pushstring(L, instance->Name.c_str());
    return 1;
}
int Instance::NameSetHandler(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    const char* name = lua_tostring(L, 3);
    instance->Name = name;
    return 0;
}

int Instance::ClassNameGetHandler(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    lua_pushstring(L, instance->ClassName.c_str());
    return 1;
}

int Instance::ParentGetHandler(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
	std::shared_ptr<Instance>& parent = instance->Parent;
    if (parent) {
        return parent->Push(L);
    }
    lua_pushnil(L);
    return 1;
}

int Instance::ParentSetHandler(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    if (lua_isnil(L, 3)) {
        if (instance->Parent)
            instance->Parent->RemoveChild(instance);
        instance->Parent = nullptr;
        return 0;
    }
    std::shared_ptr<Instance>& parent = GetFromIndex<Instance>(L, 3);
    if (parent != instance->Parent)
        instance->SetParent(parent);
    return 0;
}

void Instance::ClearChildren()
{
    while(Children.size())
        Children[0]->SetParent(nullptr);
}

int Instance::ClearChildren(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    instance->ClearChildren();
    return 0;
}

const std::unordered_map<std::string, const Method> Instance::ConcatMethods(std::unordered_map<std::string, const Method> lhs, const std::unordered_map<std::string, const Method>& rhs)
{
    lhs.insert(rhs.begin(), rhs.end());
    return lhs;
}

int Instance::__index(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    std::string field = lua_tostring(L, 2);
    auto* baseProperties = instance->getProperties();
    const auto iter = baseProperties->find(field);
    if (iter != baseProperties->end()) {
        void** dataSpace = (void**)lua_newuserdata(L, sizeof(void*));
        *dataSpace = iter->second.Data;
        return iter->second.GetHandler(L);
    };
    auto* baseMethods = instance->getMethods();
    const auto iter2 = baseMethods->find(field);
    if (iter2 != baseMethods->end())
        return iter2->second.Push<Instance>(L); /* Potential problems in future, e.g. colliding methods in InstanceMT */
    for (auto& child : instance->Children)
        if (child->Name == field)
            return child->Push(L);
    return luaL_error(L, "%s is not a valid member of %s", field.c_str(), instance->Name.c_str());
}

int Instance::__newindex(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    const char* field = lua_tostring(L, 2);
    auto* baseProperties = instance->getProperties();
    auto iter = baseProperties->find(field);
    if (iter != baseProperties->end()) {
        void** dataSpace = (void**)lua_newuserdata(L, sizeof(void*));
        *dataSpace = iter->second.Data;
        return iter->second.SetHandler(L);
    }
    return luaL_error(L, "%s is not a valid member of %s", field, instance->Name.c_str());
}

int Instance::__tostring(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    lua_pushstring(L, instance->Name.c_str());
    return 1;
}

int Instance::__eq(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    std::shared_ptr<Instance>& instance2 = GetFromIndex<Instance>(L, 2);
    lua_pushboolean(L, instance == instance2);
    return 1; 
}

int Instance::__gc(lua_State* L)
{
    std::shared_ptr<Instance>& instance = GetFromIndex<Instance>(L, 1);
    instance.reset();
    return 0;
}

void Instance::Render()
{};

void Instance::Dump(rapidjson::Value& object, rapidjson::Document::AllocatorType& allocator)
{
	object.AddMember("Name", rapidjson::Value(Name.data(), Name.size(), allocator).Move(), allocator);
	object.AddMember("ClassName", rapidjson::Value(ClassName.data(), ClassName.size(), allocator).Move(), allocator);
}
} // namespace Atmos
