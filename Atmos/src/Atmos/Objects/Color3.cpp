#include "Atmos/Objects/Color3.h"

#include <string>
#include <unordered_map>

namespace Atmos {
Color3::Color3(Number ra, Number ga, Number ba)
    : r(ra)
    , g(ga)
    , b(ba) {};

Color3::Color3(rapidjson::Value& obj)
	: r(obj[0].GetDouble())
	, g(obj[1].GetDouble())
	, b(obj[2].GetDouble())
{};

Color3 operator+(const Color3& lhs, const Color3& rhs) { return { lhs.r + rhs.r, lhs.g + rhs.g, lhs.b + rhs.b }; }
Color3 operator-(const Color3& lhs, const Color3& rhs) { return { lhs.r - rhs.r, lhs.g - rhs.g, lhs.b - rhs.b }; }
template <typename T>
Color3 operator*(const Color3& lhs, const T& rhs) { return { lhs.r * rhs, lhs.g * rhs, lhs.b * rhs }; }
Color3 operator*(const Color3& lhs, const Color3& rhs) { return { lhs.r * rhs.r, lhs.g * rhs.g, lhs.b * rhs.b }; }
template <typename T>
Color3 operator/(const Color3& lhs, const T& rhs) { return { lhs.r / rhs, lhs.g / rhs, lhs.b / rhs }; }
Color3 operator/(const Color3& lhs, const Color3& rhs) { return { lhs.r / rhs.r, lhs.g / rhs.g, lhs.b / rhs.b }; }
bool operator==(const Color3& lhs, const Color3& rhs) { return (lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b); }

void Color3::SetupState(lua_State* L)
{
    /* Metatable */
    luaL_newmetatable(L, MetatableName);

    lua_pushcfunction(L, &Color3::__index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, &Color3::__newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, &Color3::__tostring);
    lua_setfield(L, -2, "__tostring");
    lua_pushcfunction(L, &Color3::__eq);
    lua_setfield(L, -2, "__eq");
    lua_pushcfunction(L, &Color3::__add);
    lua_setfield(L, -2, "__add");
    lua_pushcfunction(L, &Color3::__sub);
    lua_setfield(L, -2, "__sub");
    lua_pushcfunction(L, &Color3::__mul);
    lua_setfield(L, -2, "__mul");
    lua_pushcfunction(L, &Color3::__div);
    lua_setfield(L, -2, "__div");

    lua_pop(L, 1);

    /* Color3 Library */
    lua_newtable(L);
    lua_pushcfunction(L, &Color3::newColor3);
    lua_setfield(L, -2, "new");
    lua_setglobal(L, "Color3");
}

int Color3::Push(lua_State* L)
{
    PushRaw(L, this);
    GetMetatable<Color3>(L);
    lua_setmetatable(L, -2);
    return 1;
}

int Color3::newColor3(lua_State* L)
{

    return Color3(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3)).Push(L);
}

/* Metamethods */

int Color3::__index(lua_State* L)
{
    static std::unordered_map<std::string, Number Color3::*> fieldMap = {
        { "r", &Color3::r },
        { "R", &Color3::r },
        { "g", &Color3::g },
        { "G", &Color3::g },
        { "b", &Color3::b },
        { "B", &Color3::b },
    };
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    std::string field = lua_tostring(L, 2);
    auto iter = fieldMap.find(field);
    if (iter != fieldMap.end()) {
        lua_pushnumber(L, col3.*iter->second);
        return 1;
    };
    return luaL_error(L, "%s is not a valid member of %s", field, MetatableName);
}

int Color3::__newindex(lua_State* L)
{
    return luaL_error(L, "%s cannot be assigned to", lua_tostring(L, 2));
}

int Color3::__tostring(lua_State* L)
{
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    lua_pushfstring(L, "%f, %f, %f", col3.r, col3.g, col3.b);
    return 1;
}

int Color3::__eq(lua_State* L)
{
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    Color3 ncol3 = RawGetFromIndex<Color3>(L, 2);
    lua_pushboolean(L, col3 == ncol3);
    return 1;
}

int Color3::__add(lua_State* L)
{
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    Color3 ncol3 = RawGetFromIndex<Color3>(L, 2);
    return (col3 + ncol3).Push(L);
}

int Color3::__sub(lua_State* L)
{
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    Color3 ncol3 = RawGetFromIndex<Color3>(L, 2);
    return (col3 - ncol3).Push(L);
}

int Color3::__mul(lua_State* L)
{
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    Color3 ncol3 = RawGetFromIndex<Color3>(L, 2);
    return (col3 * ncol3).Push(L);
}

int Color3::__div(lua_State* L)
{
    Color3 col3 = RawGetFromIndex<Color3>(L, 1);
    Color3 ncol3 = RawGetFromIndex<Color3>(L, 2);
    return (col3 / ncol3).Push(L);
}

rapidjson::Value Color3::Dump(rapidjson::Document::AllocatorType& allocator)
{
	rapidjson::Value Array(rapidjson::kArrayType);
	Array.PushBack(r, allocator);
	Array.PushBack(g, allocator);
	Array.PushBack(b, allocator);
	return Array;
}

} // namespace Atmos
