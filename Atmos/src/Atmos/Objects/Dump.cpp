#include "Atmos/Objects/Dump.h"
#include "Atmos/Atmos.h"

#include <unordered_map>
#include <string>
#include <functional>

long count = 0; // ugly but simple solution

rapidjson::Document DumpObject(std::shared_ptr<Atmos::Instance> instance, bool IncludeChildren = true, long parentNode = 0)
{
	rapidjson::Document result(rapidjson::kArrayType);
	rapidjson::Document::AllocatorType& allocator = result.GetAllocator();
	
	rapidjson::Value object(rapidjson::kObjectType);

	instance->Dump(object, allocator);

	if(parentNode) {
		object.AddMember("Parent", parentNode-1, allocator);
	}

	result.PushBack(object, allocator);
	++count;

	if(IncludeChildren) {
		long t = count;
		for(std::shared_ptr<Atmos::Instance> child : instance->Children) {
			rapidjson::Value buff = rapidjson::Value(DumpObject(child, true, t), allocator);
			if (buff.IsArray())
				for(auto& o : buff.GetArray())
					result.PushBack(o, allocator);
			else if (buff.IsObject())
				result.PushBack(buff, allocator);
		}
	}

	return result;
};

namespace Atmos
{

rapidjson::Document Dump(std::shared_ptr<Atmos::Instance> instance)
{
	rapidjson::Document result = DumpObject(instance);
	count = 0;
	return result;
};

#define GEN_CONSTRUCTOR(x) {#x, [](rapidjson::Value& obj)->std::shared_ptr<x>{return ObjectManager::Create<x>(obj);}}

std::vector<std::shared_ptr<Instance>> Undump(rapidjson::Document& dump)
{
	static std::unordered_map< std::string, std::function<std::shared_ptr<Instance>(rapidjson::Value&)> > Constructors = {
		GEN_CONSTRUCTOR(Instance),
		GEN_CONSTRUCTOR(Part),
		GEN_CONSTRUCTOR(Script)
	};

	std::vector<std::shared_ptr<Instance>> result;

	rapidjson::Document::AllocatorType& allocator = dump.GetAllocator();

	for(auto& obj : dump.GetArray())
	{
		std::string className = obj["ClassName"].GetString(); // no length check, embedded \0 not supported.
		const auto& c = Constructors.find(className);
		if(c != Constructors.end())
			result.emplace_back(c->second(obj));
		else
			result.emplace_back(Constructors["Instance"](obj));
		auto parentIter = obj.FindMember("Parent");
		if(parentIter != obj.MemberEnd())
			result.back()->SetParent(result[parentIter->value.GetInt()]);
	}

	return result;
};

}
