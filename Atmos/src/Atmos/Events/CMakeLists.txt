cmake_minimum_required(VERSION 3.10)
project(ATMOS_EVENTS)

if(NOT DEFINED ATMOS_INCLUDE)
    message(FATAL_ERROR "ATMOS_INCLUDE not defined")
endif()

file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR} *.cpp)

add_library(atmos_events OBJECT ${SRC_FILES})
target_compile_features(atmos_events PUBLIC cxx_std_17)
target_include_directories(atmos_events PRIVATE ${ATMOS_INCLUDE})
