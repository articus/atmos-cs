#include "Atmos/Events/Keyboard/KeyDown.h"

namespace Atmos
{
std::list<Atmos::Event::Handler>::const_iterator KeyDownEvent::Subscribe(Handler handler)
{
    return Handlers.emplace(Handlers.end(), handler);
}

void KeyDownEvent::Remove(std::list<Atmos::Event::Handler>::const_iterator iter)
{
    Handlers.erase(iter);
}

void KeyDownEvent::Handle()
{
    for(std::list<Atmos::Event::Handler>::const_iterator handler = Handlers.begin(); handler != Handlers.end(); ++handler)
        if(!(*handler)(this))
            Handlers.erase(handler);
}


}
