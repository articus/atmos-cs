#include "Atmos/Events/EventManager.h"

namespace Atmos
{

void EventManager::HandleEvents()
{
    while(Events.size())
    {
        Events.front()->Handle();
        Events.pop();
    }
}


}
