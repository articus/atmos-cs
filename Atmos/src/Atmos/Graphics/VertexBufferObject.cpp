#include "Atmos/Graphics/VertexBufferObject.h"

namespace Atmos
{
namespace Graphics
{

VertexBufferObject::VertexBufferObject()
{
    GLCall(glGenBuffers(1, &ID));
}

VertexBufferObject::VertexBufferObject(GLsizeiptr size, const void* data, GLenum usage)
{
    GLCall(glGenBuffers(1, &ID));
    BufferData(size, data, usage);
}

void VertexBufferObject::BufferData(GLsizeiptr size, const void* data, GLenum usage) {
    Bind();
    GLCall(glBufferData(GL_ARRAY_BUFFER, size, data, usage));
    Unbind();
}

void VertexBufferObject::SubBufferData(GLintptr offset, GLsizeiptr size, const void* data)
{
    Bind();
    GLCall(glBufferSubData(GL_ARRAY_BUFFER, offset, size, data));
    Unbind();
}

void VertexBufferObject::Bind() const
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, ID));
}

void VertexBufferObject::Unbind() const
{
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
}


VertexBufferObject::~VertexBufferObject()
{
    GLCall(glDeleteBuffers(1, &ID));
}

}
}
