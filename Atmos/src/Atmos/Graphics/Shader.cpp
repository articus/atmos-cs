#include "Atmos/Graphics/Shader.h"

namespace Atmos
{
namespace Graphics
{
Shader::Shader()
{
    GLCall(ID = glCreateProgram());
}

bool Shader::CompileShader(const char* source, GLenum shaderType)
{
    GLCall(GLuint shader = glCreateShader(shaderType));
    GLCall(glShaderSource(shader, 1, &source, NULL));
    GLCall(glCompileShader(shader));
    int success;
    char infoLog[512];
    GLCall(glGetShaderiv(shader, GL_COMPILE_STATUS, &success));
    if(!success)
    {
        GLCall(glGetShaderInfoLog(shader, 512, NULL, infoLog));
        assert(0);
        return false;
    };
    shaders.emplace_back(shader);
    return true;
}

bool Shader::LinkProgram()
{
    for(GLuint shader : shaders) {
        GLCall(glAttachShader(ID, shader));
        GLCall(glDeleteShader(shader));
    };
    GLCall(glLinkProgram(ID));
    int success;
    char infoLog[512];
    if(!success)
    {
        GLCall(glGetProgramInfoLog(ID, 512, nullptr, infoLog));
        assert(0);
        return false;
    };
    return true;
}

//SetInt, SetVec3 etc

void Shader::Bind() const
{
    GLCall(glUseProgram(ID));
}

void Shader::Unbind() const
{
    GLCall(glUseProgram(0));
}

Shader::~Shader()
{
    GLCall(glDeleteProgram(ID));
}

}
}
