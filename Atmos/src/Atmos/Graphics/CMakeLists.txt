cmake_minimum_required(VERSION 3.10)
project(ATMOS_GRAPHICS)

if(NOT DEFINED ATMOS_INCLUDE)
    message(FATAL_ERROR "ATMOS_INCLUDE not defined")
endif()

find_package(PkgConfig REQUIRED)
pkg_search_module(EPOXY REQUIRED IMPORTED_TARGET epoxy)

add_library(atmos_graphics OBJECT OpenGL.cpp Shader.cpp VertexBufferObject.cpp)
target_include_directories(atmos_graphics PRIVATE ${ATMOS_INCLUDE})

target_link_libraries(atmos_graphics PUBLIC PkgConfig::EPOXY)
