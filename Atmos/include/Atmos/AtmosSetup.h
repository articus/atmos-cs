#pragma once

#include <epoxy/gl.h>
extern "C" {
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
}

#include <cassert>
#include <memory>

namespace Atmos {
typedef lua_Number Number;

template <typename T>
void GetMetatable(lua_State* L)
{
    luaL_getmetatable(L, T::MetatableName);
    if (lua_type(L, -1) == LUA_TNIL) {
        lua_pop(L, 1);
        T::SetupState(L);
        luaL_getmetatable(L, T::MetatableName);
    }
}

template <typename T>
int PushRaw(lua_State* L, T* obj)
{
    T* ptr = static_cast<T*>(lua_newuserdata(L, sizeof(T)));
    *ptr = *obj;
    return 1;
}

template <typename T>
T RawGetFromIndex(lua_State* L, int index)
{
    void* ptr = lua_touserdata(L, index);
    if (ptr != NULL) {
        return *static_cast<T*>(ptr);
    }
    luaL_typerror(L, index, T::MetatableName);
    /* Avoid warnings */
    return *(T*)nullptr;
}

template <typename T>
std::shared_ptr<T>& GetFromIndex(lua_State* L, int index)
{
    void* ptr = lua_touserdata(L, index);
    if (ptr != NULL) {
        return *static_cast<std::shared_ptr<T>*>(ptr);
    }
    luaL_typerror(L, index, T::MetatableName);
    /* Avoid warnings */
    static std::shared_ptr<T> tmp { nullptr };
    return tmp;
}
} // namespace Atmos

#include "Atmos/Objects/ObjectManager.h"
#include <iostream>
