#include "Atmos/Events/Event.h"

#include "Atmos/Objects/Instance.h"

namespace Atmos
{
    struct ChildAddedEvent : Event
    {
        Instance* Parent;
        Instance* Child;
        virtual std::list<Handler>::const_iterator Subscribe(Handler handler) override;
        virtual void Remove(std::list<Handler>::const_iterator) override;
        virtual void Handle() override;
    private:
        inline static std::list<Handler> Handlers;
    };
}
