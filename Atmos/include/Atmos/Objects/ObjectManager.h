#pragma once

#include <memory>
#include <new>

#include "Atmos/AtmosSetup.h"

namespace Atmos {
class ObjectManager {
public:
    template <typename T, typename... Arg>
    static std::shared_ptr<T> Create(Arg&&... args)
    {
        return std::make_shared<T>(std::forward<Arg>(args)...);
    };
};
} // namespace Atmos
