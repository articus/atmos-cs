#pragma once

#include "Atmos/AtmosSetup.h"

#include <functional>
#include <string>

namespace Atmos {
class Method {
public:
    const std::string Name;
    const std::function<int(lua_State*)> Func;
    Method() = default;
    Method(const std::string& name, int (*function)(lua_State*))
        : Name(name)
        , Func(function) {};
    static int Wrapper(lua_State* L);
    void Register(lua_State* L) const;
    template <typename T>
    void Register(lua_State* L) const
    {
        GetMetatable<T>(L);
        Register(L);
        lua_pop(L, 1);
    }
    template <typename T>
    int Push(lua_State* L) const
    {
        GetMetatable<T>(L);
        lua_getfield(L, -1, Name.c_str());
        assert(!lua_isnil(L, -1));
        lua_remove(L, -2);
        return 1;
    }
};
}; // namespace Atmos
