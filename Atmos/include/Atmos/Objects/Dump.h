#pragma once

#include "Atmos/Objects/ObjectManager.h"
#include "Atmos/Objects/Instance.h"

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>

#include <memory>

namespace Atmos
{
	rapidjson::Document Dump(std::shared_ptr<Atmos::Instance> instance);

	std::vector<std::shared_ptr<Atmos::Instance>> Undump(rapidjson::Document& dump);
}
