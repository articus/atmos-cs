#pragma once

#include "Atmos/Objects/Instance.h"

// Todo: Make some sort of global Atmos object, Atmos::Initiate etc, so that the main lua_State* L is not exposed directly and can be managed.

namespace Atmos
{
class Script : public Instance {
public:
	Script();
	Script(const std::string& name);
	Script(rapidjson::Value& obj);

	int Compile(lua_State* L, std::string source);
	int Run(lua_State* L);
	virtual void Dump(rapidjson::Value& object, rapidjson::Document::AllocatorType& allocator) override;
private:
	std::string Bytecode;
	static int writer(lua_State* L, const void* lBuffer, size_t sizeLBuffer, void* oBuffer);
};

}

