#pragma once

#include "Atmos/AtmosSetup.h"
#include "Atmos/Objects/Method.h"

#include <string>
#include <unordered_map>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>

namespace Atmos {
struct Color3 {
    Number r, g, b = 0;
    Color3() = default;
    Color3(Number ra, Number ga, Number ba);
    Color3(const Color3&) = default;
	Color3(rapidjson::Value& obj);
    Color3& operator=(const Color3&) = default;
    // ~Color3() { std::cout << "Color3 destructor." << std::endl; }

    // Color3 operator+(const Color3& rhs); Written outside of class to allow lhs conversion
    // Color3 operator-(const Color3& rhs);
    // Color3 operator*(const Color3& rhs);
    // Color3 operator/(const Color3& rhs);
    // bool operator==(const Color3& rhs);

    constexpr static const char* const MetatableName = "Color3";
    static void SetupState(lua_State* L);
    int Push(lua_State* L);
    static int newColor3(lua_State* L);

    /* Metamethods */
    static int __index(lua_State* L);
    static int __newindex(lua_State* L);
    static int __tostring(lua_State* L);
    static int __eq(lua_State* L);
    static int __gc(lua_State* L);
    static int __add(lua_State* L);
    static int __sub(lua_State* L);
    static int __mul(lua_State* L);
    static int __div(lua_State* L);
	
	rapidjson::Value Dump(rapidjson::Document::AllocatorType& allocator);

};
}
