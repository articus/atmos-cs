#pragma once

#include "Atmos/AtmosSetup.h"
#include "Atmos/Objects/Method.h"
#include "Atmos/Objects/Property.h"
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#define GEN_PROPERTY(name) { #name, { #name, nullptr, name##GetHandler, name##SetHandler } }

namespace Atmos {
class Method;
class Instance : public std::enable_shared_from_this<Instance> {
public:
    std::string Name = "Instance";
    std::string ClassName = "Instance";
    std::shared_ptr<Instance> Parent = nullptr;
    Instance() = default;
    Instance(const std::string& name);
    Instance(const std::string& name, const std::string& className);
	Instance(rapidjson::Value& obj);
    ~Instance() { std::cout << "Instance destructor." << std::endl; }
    std::shared_ptr<Instance> GetPointer();

    std::vector<std::shared_ptr<Instance>> Children;
    void AddChild(const std::shared_ptr<Instance>& child);
    void RemoveChild(const std::shared_ptr<Instance>& child);
    void SetParent(const std::shared_ptr<Instance>& parent);
    void RemoveParent();

    constexpr static const char* const MetatableName = "Instance";
    static void SetupState(lua_State* L);
    virtual int Push(lua_State* L);

    inline static std::unordered_map<std::string, std::function<int(lua_State*)>> newRegistrars = {};
    static int newInstance(lua_State* L);

    /* Properties */
private:
    static int NameGetHandler(lua_State* L);
    static int NameSetHandler(lua_State* L);
    static int ClassNameGetHandler(lua_State* L);
    static int ParentGetHandler(lua_State* L);
    static int ParentSetHandler(lua_State* L);

protected:
    inline static const std::unordered_map<std::string, const Property> Properties = {
		GEN_PROPERTY(Name),
        { "ClassName", { "ClassName", nullptr, Instance::ClassNameGetHandler, nullptr } },
		GEN_PROPERTY(Parent)
    };
    virtual const std::unordered_map<std::string, const Property>* const getProperties() const { return &Properties; };
    static const std::unordered_map<std::string, const Property> ConcatProperties(std::unordered_map<std::string, const Property> lhs, const std::unordered_map<std::string, const Property>& rhs);

    /* Methods */
public:
    void ClearChildren();
    static int ClearChildren(lua_State* L);

protected:
    inline static const std::unordered_map<std::string, const Method> Methods = {
        { "ClearChildren", { "ClearChildren", ClearChildren } },
    };
    virtual const std::unordered_map<std::string, const Method>* const getMethods() const { return &Methods; };
    static const std::unordered_map<std::string, const Method> ConcatMethods(std::unordered_map<std::string, const Method> lhs, const std::unordered_map<std::string, const Method>& rhs);

private:
    /* Metafields */
    static int __index(lua_State* L);
    static int __newindex(lua_State* L);
    static int __tostring(lua_State* L);
    static int __eq(lua_State* L);
    static int __gc(lua_State* L);
public:
    virtual void Render();
	
	virtual void Dump(rapidjson::Value& object, rapidjson::Document::AllocatorType& allocator);
	std::shared_ptr<Atmos::Instance> Undump();
protected:
	using BaseType = Instance; // https://www.fluentcpp.com/2017/12/26/emulate-super-base/
};
} // namespace Atmos

#define GEN_REGISTRAR(name) Instance::newRegistrars[#name] = \
								[](lua_State* L) -> int {\
								return ObjectManager::Create<name>()->Push(L);\
							}
