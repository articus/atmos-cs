#pragma once

#include "Atmos/AtmosSetup.h"

#include <functional>
#include <string>

namespace Atmos {
struct Property {
    std::string Name;
    void* Data;
    std::function<int(lua_State*)> GetHandler;
    std::function<int(lua_State*)> SetHandler;
    Property() = delete;
    Property(const std::string& name, void* data, const std::function<int(lua_State*)>& getHandler, const std::function<int(lua_State*)>& setHandler);
    static int DefaultGetHandler(lua_State* L);
    static int DefaultSetHandler(lua_State* L);
};
}
