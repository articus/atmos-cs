#pragma once

#include "Atmos/AtmosSetup.h"
#include "Atmos/Objects/Method.h"

#include <string>
#include <unordered_map>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>

namespace Atmos {
struct Vector3 {
    Number x, y, z = 0;
    Vector3() = default;
    Vector3(Number xa, Number ya, Number za);
    Vector3(const Vector3&) = default;
	Vector3(rapidjson::Value& obj);
    Vector3& operator=(const Vector3&) = default;
    // ~Vector3() { std::cout << "Vector3 destructor." << std::endl; }

    // Vector3 operator+(const Vector3& rhs); Written outside of class to allow lhs conversion
    // Vector3 operator-(const Vector3& rhs);
    // Vector3 operator*(const Vector3& rhs);
    // Vector3 operator/(const Vector3& rhs);
    // bool operator==(const Vector3& rhs);

    constexpr static const char* const MetatableName = "Vector3";
    static void SetupState(lua_State* L);
    int Push(lua_State* L);
    static int newVector3(lua_State* L);

    /* Methods */
    Number Magnitude();
    Vector3 Unit();

    Vector3 Lerp(Vector3 goal, Number alpha);
    static int Lerp(lua_State* L);
    Number Dot(Vector3 other);
    static int Dot(lua_State* L);
    Vector3 Cross(Vector3 other);
    static int Cross(lua_State* L);

    inline static const std::unordered_map<std::string, Method> Methods = {
        { "Lerp", { "Lerp", Lerp } },
        { "Dot", { "Dot", Dot } },
        { "Cross", { "Cross", Cross } }
    };

    /* Metamethods */
    static int __index(lua_State* L);
    static int __newindex(lua_State* L);
    static int __tostring(lua_State* L);
    static int __eq(lua_State* L);
    static int __gc(lua_State* L);
    static int __add(lua_State* L);
    static int __sub(lua_State* L);
    static int __mul(lua_State* L);
    static int __div(lua_State* L);

	rapidjson::Value Dump(rapidjson::Document::AllocatorType& allocator);
};
}
