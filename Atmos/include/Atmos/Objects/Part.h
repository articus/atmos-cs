#pragma once

#include "Atmos/Objects/Instance.h"
#include "Atmos/Objects/Color3.h"
#include "Atmos/Objects/Vector3.h"

namespace Atmos {
class Part : public Instance {
public:
    Vector3 Size = { 4, 2, 2 };
    Vector3 Position = { 0, 0, 0 };
    Vector3 Rotation = { 0, 0, 0 };
    Color3 Color = { 255.0 / 255.0, 192.0 / 255.0, 203.0 / 255.0 };
    Part();
    Part(const std::string& name);
	Part(rapidjson::Value& obj);
    ~Part() { std::cout << "Part destructor." << std::endl; }
private:
    static void Setup(); // to be called using std::call_once. push a newRegistrar into Instance::newRegistrars;

    static int SizeGetHandler(lua_State* L);
    static int SizeSetHandler(lua_State* L);
    static int PositionGetHandler(lua_State* L);
    static int PositionSetHandler(lua_State* L);
    static int RotationGetHandler(lua_State* L);
    static int RotationSetHandler(lua_State* L);
    static int ColorGetHandler(lua_State* L);
    static int ColorSetHandler(lua_State* L);
protected:
    inline static const std::unordered_map<std::string, const Property> Properties = Instance::ConcatProperties(
        Instance::Properties, { 
			GEN_PROPERTY(Size),
			GEN_PROPERTY(Position),
			GEN_PROPERTY(Rotation),
			GEN_PROPERTY(Color)
        });
    virtual const std::unordered_map<std::string, const Property>* const getProperties() const override { return &Properties; };
private:
    inline static const char* VertexShaderSource = R"(
        #version 330 core
        layout (location = 0) in vec3 aPos;
        void main()
            gl_position = vec4(aPos, 1.0);
        }
    )";

    inline static const char* FragmentShaderSource = R"(
        #version 330 core
        out vec4 FragColor;
        void main()
            FragColor = vec4(1.0f, 0.5f 0.2f, 1.0f);
        }
    )";
    inline static unsigned int ShaderProgram = 0;
    inline static unsigned int VBO = 0;
    inline static unsigned int VAO = 0;
public:
    virtual void Render() override;

	virtual void Dump(rapidjson::Value& object, rapidjson::Document::AllocatorType& allocator) override;
};
}
