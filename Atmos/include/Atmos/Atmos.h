#pragma once

#include "Atmos/AtmosSetup.h"

/* Objects */
#include "Atmos/Objects/Instance.h"
#include "Atmos/Objects/Script.h"
#include "Atmos/Objects/Vector3.h"
#include "Atmos/Objects/Part.h"
