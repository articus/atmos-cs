#pragma once

#include "OpenGL.h"

namespace Atmos
{
namespace Graphics
{
    class VertexBufferObject : public OpenGLObject
    {
    public:
        VertexBufferObject();
        VertexBufferObject(GLsizeiptr size, const void* data, GLenum usage);
        void BufferData(GLsizeiptr size, const void* data, GLenum usage);
        void SubBufferData(GLintptr offset, GLsizeiptr size, const void* data);

        void Bind() const;
        void Unbind() const;
        ~VertexBufferObject();
    };
    using VBO = VertexBufferObject;
}
}
