#pragma once

#include <epoxy/gl.h>

namespace Atmos {
namespace Graphics {

    class OpenGLObject { // Blueprint, may also allows custom properties/ property swaps at runtime.
    public:
        virtual void Bind() const = 0;
        virtual void Unbind() const = 0;
        virtual ~OpenGLObject() = 0;
        inline GLuint GetId() const { return ID; };
    protected:
        GLuint ID;
    };

}
}

#include <cassert>
#define GLCall(x) x; assert(!glGetError())
