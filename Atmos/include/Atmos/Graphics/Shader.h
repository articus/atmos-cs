#pragma once

#include "OpenGL.h"

#include <vector>

namespace Atmos
{
namespace Graphics
{
    class Shader : public OpenGLObject
    {
    public:
        Shader();
        bool CompileShader(const char* source, GLenum shaderType);
        bool LinkProgram();

        void Bind() const;
        void Unbind() const;
        ~Shader();
    private:
        std::vector<GLuint> shaders;
    };
}
}
