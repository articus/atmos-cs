#pragma once
//https://medium.com/@savas/nomad-game-engine-part-7-the-event-system-45a809ccb68f

#include <list>
#include <functional>

namespace Atmos
{

struct Event
{
    using Handler = std::function<bool(Event*)>;

    bool Handled = false; // Loop through Handlers or not.
    virtual std::list<Handler>::const_iterator Subscribe(Handler handler) = 0; // Callback returns true if should carry on being used, or gets detached. Subscribe returns ID to remove with.
    virtual void Remove(std::list<Handler>::const_iterator) = 0;
    virtual void Handle() = 0; // Handle all events.
protected:
    static std::list<Handler> Handlers;
};

}
