#include "Atmos/Events/Event.h"

namespace Atmos
{
    struct KeyDownEvent : Event
    {
        unsigned int keycode;
        virtual std::list<Handler>::const_iterator Subscribe(Handler handler) override;
        virtual void Remove(std::list<Handler>::const_iterator) override;
        virtual void Handle() override;
    private:
        inline static std::list<Handler> Handlers;
    };
}
