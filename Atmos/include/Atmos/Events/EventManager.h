#pragma once

#include "Atmos/Events/Event.h"

#include <memory>
#include <queue>

namespace Atmos
{

class EventManager
{
public:
    template<typename T, typename ...Args>
    static void AddEvent(Args&&... args)
    {
        Events.emplace(std::make_unique<T>(std::forward(args)...));
    };
    static void HandleEvents();
private:
    static std::queue<std::unique_ptr<Event>> Events;
};

}
