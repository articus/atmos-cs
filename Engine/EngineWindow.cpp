#include "EngineWindow.h"

#include <cstdio>

#include <epoxy/gl.h>

#include <random>

std::random_device dev;
std::mt19937 rng(dev());
std::uniform_real_distribution<float> dist(0, 1);

EngineWindow::EngineWindow(const std::shared_ptr<Atmos::Instance>& game, std::function<void(void)> setupWithOGL, std::function<void(void)> render)
    : Game(game),
    SetupWithOGL(setupWithOGL),
    Render(render)
{
    set_border_width(10);
    add_tick_callback(sigc::mem_fun(*this, &EngineWindow::tickCallback));
    m_GLArea.set_required_version(3, 3);
    m_GLArea.signal_realize().connect(sigc::mem_fun(*this, &EngineWindow::realizeCallback));
    signal_realize().connect(sigc::mem_fun(*this, &EngineWindow::realizeCallback2));
    m_GLArea.signal_render().connect(sigc::mem_fun(*this, &EngineWindow::renderCallback));

    m_GLArea.set_auto_render(true);
    m_GLArea.property_auto_render().set_value(true);

    add(m_GLArea);
    m_GLArea.show();
}

EngineWindow::~EngineWindow()
{

}

void EngineWindow::realizeCallback()
{
g_message("cback");
m_GLArea.make_current();
SetupWithOGL();
}

void EngineWindow::realizeCallback2()
{
g_message("cback2");
}

bool EngineWindow::tickCallback(const Glib::RefPtr<Gdk::FrameClock>& frame_clock)
{
    m_GLArea.queue_render();
    return true;
}

bool EngineWindow::renderCallback(const Glib::RefPtr<Gdk::GLContext>& context)
{
    /*
    constexpr auto renderChildren = [](std::shared_ptr<Atmos::Instance>& child) {
        child->Draw();
        for(std::shared_ptr<Atmos::Instance>& chld : child->Children)
            renderChildren(chld);
    }
    */
    glClearColor(0, dist(rng), 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    Render();
    return false;
}
