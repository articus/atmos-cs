#pragma once

#include "Atmos/Atmos.h"

#include <gtkmm/glarea.h>
#include <gtkmm/window.h>

#include <functional>

class EngineWindow : public Gtk::Window
{
public:
    std::shared_ptr<Atmos::Instance> Game;
    EngineWindow(const std::shared_ptr<Atmos::Instance>& game, std::function<void(void)> setupWithOGL, std::function<void(void)> render);
    virtual ~EngineWindow();
protected:
    void realizeCallback();
    void realizeCallback2();
    bool tickCallback(const Glib::RefPtr<Gdk::FrameClock>& frame_clock);
    bool renderCallback(const Glib::RefPtr<Gdk::GLContext>& context);
private:
    std::function<void(void)> Setup;
    std::function<void(void)> SetupWithOGL;
    std::function<void(void)> Render;
public:
    Gtk::GLArea m_GLArea;
};
