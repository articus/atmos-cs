#include "Atmos/Atmos.h"
#include "EngineWindow.h"

#include <cstdio>
#include <iostream>
#include <thread>
#include <filesystem>

#include <epoxy/gl.h>

#include <readline/readline.h>
#include <readline/history.h>

#include <gtkmm/application.h>
int main(int argc, char* argv[])
{
	std::filesystem::current_path(std::filesystem::path(argv[0]).parent_path());
    
	auto app = Gtk::Application::create(argc, argv, "test");
    int error;
    lua_State* L = lua_open();
    luaL_openlibs(L);
    std::shared_ptr<Atmos::Instance> Environment = Atmos::ObjectManager::Create<Atmos::Instance>("Environment", "Environment");
    Environment->Push(L);
    lua_setglobal(L, "env");
    EngineWindow window(
        Environment, [&] {
  std::shared_ptr<Atmos::Part> Part = Atmos::ObjectManager::Create<Atmos::Part>("Part");
  Part->Push(L);
  lua_setglobal(L, "part");
  Part->SetParent(Environment); }, [&] {
     auto renderChildren = [&](std::shared_ptr<Atmos::Instance> instance, const auto rndFunc) -> void {
            instance->Render();
            for(std::shared_ptr<Atmos::Instance>& child : instance->Children)
                rndFunc(child, rndFunc);
        };
        renderChildren(Environment, renderChildren);
        }
    );
    const char* version = (const char*)glGetString(GL_VERSION);
    freopen("CONOUT$", "w", stdout);
    std::cout << "Hello!" << (void*)version << version << std::endl;
    std::thread thread1([&]() { app->run(window); });
    freopen("CONOUT$", "w", stdout);
    printf("%s: %s\n", "Main thread", (const char*)glGetString(GL_VERSION));
	char* buf;
    while ((readline(buf)) != nullptr) {
		if(strlen(buf)) add_history(buf);
        error = luaL_loadbuffer(L, buf, strlen(buf), "client") || lua_pcall(L, 0, 0, 0);
        if (error) {
            fprintf(stderr, "%s\n", lua_tostring(L, -1));
            lua_pop(L, 1);
        }
		free(buf);
    }
    lua_close(L);
    return 0;
}
